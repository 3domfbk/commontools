package geom

import "math"

// Ray is a half line with specified Origin and Direction
type Ray struct {
	Origin, Direction Vec3
}

const (
	epsilon = 0.000000001
)

// NewRay construct a new normalized ray
func NewRay(origin Vec3, direction Vec3) Ray {
	return Ray{Origin: origin, Direction: direction.Nor()}
}

// IntersectsAABB performs ray intersection between ray and AABB
func (r Ray) IntersectsAABB(aabb *AABB) bool {
	dx := r.Direction.X
	if dx == 0 {
		dx = epsilon
	}
	dy := r.Direction.Y
	if dy == 0 {
		dy = epsilon
	}
	dz := r.Direction.Z
	if dz == 0 {
		dz = epsilon
	}

	t1 := (aabb.Min.X - r.Origin.X) / dx
	t2 := (aabb.Max.X - r.Origin.X) / dx
	t3 := (aabb.Min.Y - r.Origin.Y) / dy
	t4 := (aabb.Max.Y - r.Origin.Y) / dy
	t5 := (aabb.Min.Z - r.Origin.Z) / dz
	t6 := (aabb.Max.Z - r.Origin.Z) / dz

	// Magically compute something
	tmin := math.Max(math.Max(math.Min(t1, t2), math.Min(t3, t4)), math.Min(t5, t6))
	tmax := math.Min(math.Min(math.Max(t1, t2), math.Max(t3, t4)), math.Max(t5, t6))

	if tmax < 0 || tmax < tmin {
		return false
	}

	return true
}

// IntersectsTriangle performs ray intersection with a triangle
func (r Ray) IntersectsTriangle(triangle Triangle) float64 {
	e1 := triangle.Vertices[1].Sub(triangle.Vertices[0])
	e2 := triangle.Vertices[2].Sub(triangle.Vertices[0])
	P := Cross(r.Direction, e2)
	det := Dot(e1, P)
	if det > -epsilon && det < epsilon {
		return -1
	}

	invDet := 1.0 / det
	T := r.Origin.Sub(triangle.Vertices[0])
	u := Dot(T, P) * invDet
	if u < 0 || u > 1 {
		return -1
	}

	Q := Cross(T, e1)
	v := Dot(r.Direction, Q) * invDet
	if v < 0 || u+v > 1 {
		return -1
	}

	t := Dot(e2, Q) * invDet
	if t > epsilon {
		return t
	}

	return -1
}
