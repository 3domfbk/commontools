package geom

import "math"

// AABB is the Axis Aligned Bounding Box of the Faces
type AABB struct {
	Min, Max Vec3
}

// NewAABB creates an AABB
func NewAABB() *AABB {
	boundingBox := AABB{
		Min: Vec3{math.MaxFloat64, math.MaxFloat64, math.MaxFloat64},
		Max: Vec3{-math.MaxFloat64, -math.MaxFloat64, -math.MaxFloat64}}

	return &boundingBox
}

// MergeAABB merges two AABB
func MergeAABB(current, other *AABB) *AABB {
	boundingBox := AABB{
		Vec3{math.Min(current.Min.X, other.Min.X), math.Min(current.Min.Y, other.Min.Y), math.Min(current.Min.Z, other.Min.Z)},
		Vec3{math.Max(current.Max.X, other.Max.X), math.Max(current.Max.Y, other.Max.Y), math.Max(current.Max.Z, other.Max.Z)}}

	return &boundingBox
}

// Update AABB with a new point
func (aabb *AABB) Update(point Vec3) {
	aabb.Min.X = math.Min(aabb.Min.X, point.X)
	aabb.Min.Y = math.Min(aabb.Min.Y, point.Y)
	aabb.Min.Z = math.Min(aabb.Min.Z, point.Z)
	aabb.Max.X = math.Max(aabb.Max.X, point.X)
	aabb.Max.Y = math.Max(aabb.Max.Y, point.Y)
	aabb.Max.Z = math.Max(aabb.Max.Z, point.Z)
}

// Center of the AABB
func (aabb *AABB) Center() Vec3 {
	return aabb.Max.Sub(aabb.Min).Scl(0.5)
}
