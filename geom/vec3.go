package geom

import "math"

// Vec3 is a 3D vector
type Vec3 struct {
	X, Y, Z float64
}

// Sub a vector from another
func (v Vec3) Sub(v2 Vec3) Vec3 {
	return Vec3{X: v.X - v2.X, Y: v.Y - v2.Y, Z: v.Z - v2.Z}
}

// Scl a vector by a scalar
func (v Vec3) Scl(scalar float64) Vec3 {
	return Vec3{X: v.X * scalar, Y: v.Y * scalar, Z: v.Z * scalar}
}

// Dst to another vector
func (v Vec3) Dst(v2 Vec3) float64 {
	dx := v2.X - v.X
	dy := v2.Y - v.Y
	dz := v2.Z - v.Z
	return math.Sqrt(dx*dx + dy*dy + dz*dz)
}

// SqDst to another vector
func (v Vec3) SqDst(v2 Vec3) float64 {
	dx := v2.X - v.X
	dy := v2.Y - v.Y
	dz := v2.Z - v.Z
	return dx*dx + dy*dy + dz*dz
}

// Len is the length of the vector
func (v Vec3) Len() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y + v.Z*v.Z)
}

// Nor calculates the normalized vector
func (v Vec3) Nor() Vec3 {
	len := v.Len()
	return Vec3{v.X / len, v.Y / len, v.Z / len}
}

// Cross product
func Cross(v1 Vec3, v2 Vec3) Vec3 {
	x := v1.Y*v2.Z - v1.Z*v2.Y
	y := v2.X*v1.Z - v2.Z*v1.X
	z := v1.X*v2.Y - v1.Y*v2.X
	return Vec3{x, y, z}
}

// Dot product
func Dot(v1 Vec3, v2 Vec3) float64 {
	return v1.X*v2.X + v1.Y*v2.Y + v1.Z*v2.Z
}
