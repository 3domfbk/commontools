package geom_test

import (
	"testing"

	. "bitbucket.org/3domfbk/commontools/geom"
	. "github.com/smartystreets/goconvey/convey"
)

func TestMergeAABB(t *testing.T) {
	Convey("Given 2 AABBs", t, func() {

		aabbA := AABB{Min: Vec3{-11.0, -1.0, -7.0}, Max: Vec3{1.0, 3.0, 4.0}}
		aabbB := AABB{Min: Vec3{-8.0, -3.0, -1.0}, Max: Vec3{2.0, 7.0, 5.0}}

		Convey("It should correctly be able to merge them", func() {
			merged := MergeAABB(&aabbA, &aabbB)

			So(merged.Min.X, ShouldEqual, -11.0)
			So(merged.Min.Y, ShouldEqual, -3.0)
			So(merged.Min.Z, ShouldEqual, -7.0)
			So(merged.Max.X, ShouldEqual, 2.0)
			So(merged.Max.Y, ShouldEqual, 7.0)
			So(merged.Max.Z, ShouldEqual, 5.0)
		})
	})
}

type rayTest struct {
	ray        Ray
	aabb       AABB
	intersects bool
}

type rayTriTest struct {
	ray        Ray
	tri        Triangle
	intersects bool
}

func TestRayAABB(t *testing.T) {
	tests := []rayTest{
		rayTest{
			ray:        NewRay(Vec3{X: 0.5, Y: 0.25, Z: 2}, Vec3{X: 0, Y: 0, Z: -1}),
			aabb:       AABB{Min: Vec3{X: 0, Y: 0, Z: 0}, Max: Vec3{X: 1, Y: 0.5, Z: 1.6}},
			intersects: true,
		},
		rayTest{
			ray:        NewRay(Vec3{X: 1.5, Y: 0.25, Z: 2}, Vec3{X: 0, Y: 0, Z: -1}),
			aabb:       AABB{Min: Vec3{X: 0, Y: 0, Z: 0}, Max: Vec3{X: 1, Y: 0.5, Z: 1.6}},
			intersects: false,
		},
		rayTest{
			ray:        NewRay(Vec3{X: 0.5, Y: 0.51, Z: 2}, Vec3{X: 0, Y: 0, Z: -1}),
			aabb:       AABB{Min: Vec3{X: 0, Y: 0, Z: 0}, Max: Vec3{X: 1, Y: 0.5, Z: 1.6}},
			intersects: false,
		},
		rayTest{
			ray:        NewRay(Vec3{X: 0.5, Y: 0.49, Z: 2}, Vec3{X: 0, Y: 0, Z: -1}),
			aabb:       AABB{Min: Vec3{X: 0, Y: 0, Z: 0}, Max: Vec3{X: 1, Y: 0.5, Z: 1.6}},
			intersects: true,
		},
		rayTest{
			ray:        NewRay(Vec3{X: 0.5, Y: -1, Z: 0.5}, Vec3{X: 0, Y: 1, Z: 0}),
			aabb:       AABB{Min: Vec3{X: 0, Y: 0, Z: 0}, Max: Vec3{X: 1, Y: 0.5, Z: 1.6}},
			intersects: true,
		},
		rayTest{
			ray:        NewRay(Vec3{X: 0.5, Y: -1, Z: 0.5}, Vec3{X: 0, Y: 1, Z: -0.2}),
			aabb:       AABB{Min: Vec3{X: 0, Y: 0, Z: 0}, Max: Vec3{X: 1, Y: 0.5, Z: 1.6}},
			intersects: true,
		},
		rayTest{
			ray:        NewRay(Vec3{X: 0.5, Y: -1, Z: 0.5}, Vec3{X: 0, Y: 1, Z: 1.2}),
			aabb:       AABB{Min: Vec3{X: 0, Y: 0, Z: 0}, Max: Vec3{X: 1, Y: 0.5, Z: 1.6}},
			intersects: false,
		},
		rayTest{
			ray:        NewRay(Vec3{X: 0.5, Y: -1, Z: 0.5}, Vec3{X: 0, Y: 1, Z: 1}),
			aabb:       AABB{Min: Vec3{X: 0, Y: 0, Z: 0}, Max: Vec3{X: 1, Y: 0.5, Z: 1.6}},
			intersects: true,
		},
	}

	Convey("Given some Ray and some AABB, it should correctly intersect some of them", t, func() {
		for _, test := range tests {
			So(test.ray.IntersectsAABB(&test.aabb), ShouldEqual, test.intersects)
		}
	})
}

func TestRayTriangle(t *testing.T) {
	triangle := Triangle{Vertices: [3]Vec3{Vec3{X: 0, Y: 0, Z: 0}, Vec3{X: 1, Y: 0, Z: 0}, Vec3{X: 0.5, Y: 0.5, Z: 1.6}}}

	tests := []rayTriTest{
		rayTriTest{
			ray:        NewRay(Vec3{X: 0.5, Y: 0.25, Z: 2}, Vec3{X: 0, Y: 0, Z: -1}),
			tri:        triangle,
			intersects: true,
		},
		rayTriTest{
			ray:        NewRay(Vec3{X: 1.5, Y: 0.25, Z: 2}, Vec3{X: 0, Y: 0, Z: -1}),
			tri:        triangle,
			intersects: false,
		},
		rayTriTest{
			ray:        NewRay(Vec3{X: 0.5, Y: 0.51, Z: 2}, Vec3{X: 0, Y: 0, Z: -1}),
			tri:        triangle,
			intersects: false,
		},
		rayTriTest{
			ray:        NewRay(Vec3{X: 0.5, Y: 0.49, Z: 2}, Vec3{X: 0, Y: 0, Z: -1}),
			tri:        triangle,
			intersects: true,
		},
		rayTriTest{
			ray:        NewRay(Vec3{X: 0.5, Y: -1, Z: 0.5}, Vec3{X: 0, Y: 1, Z: 0}),
			tri:        triangle,
			intersects: true,
		},
		rayTriTest{
			ray:        NewRay(Vec3{X: 0.5, Y: -1, Z: 0.5}, Vec3{X: 0, Y: 1, Z: -0.2}),
			tri:        triangle,
			intersects: true,
		},
		rayTriTest{
			ray:        NewRay(Vec3{X: 0.5, Y: -1, Z: 0.5}, Vec3{X: 0, Y: 1, Z: 1.2}),
			tri:        triangle,
			intersects: false,
		},
		rayTriTest{
			ray:        NewRay(Vec3{X: 0.5, Y: -1, Z: 0.5}, Vec3{X: 0, Y: 1, Z: 1}),
			tri:        triangle,
			intersects: false,
		},
	}

	Convey("Given some Ray and some AABB, it should correctly intersect some of them", t, func() {
		for _, test := range tests {
			if test.intersects {
				So(test.ray.IntersectsTriangle(test.tri), ShouldBeGreaterThan, -1)
			} else {
				So(test.ray.IntersectsTriangle(test.tri), ShouldEqual, -1)
			}
		}
	})
}
